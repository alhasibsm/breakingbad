package com.hasib.guskitchen.di

import android.content.Context
import com.hasib.guskitchen.ui.characters.adapter.CharacterAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ActivityComponent::class)
object AppModule {

    @Provides
    fun provideCharacterAdapter(@ApplicationContext context: Context): CharacterAdapter =
        CharacterAdapter(context)
}