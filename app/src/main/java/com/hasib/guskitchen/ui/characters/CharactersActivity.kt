package com.hasib.guskitchen.ui.characters

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.hasib.guskitchen.databinding.ActivityCharactersBinding
import com.hasib.guskitchen.ui.characters.adapter.CharacterAdapter
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class CharactersActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCharactersBinding
    private val viewModel: CharacterViewModel by viewModels()
    @Inject
    lateinit var characterAdapter: CharacterAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCharactersBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.charactersList.layoutManager = LinearLayoutManager(this)
        binding.charactersList.adapter = characterAdapter

        viewModel.errorResult.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            Timber.e(it)
        })

        viewModel.characters.observe(this, {
            characterAdapter.submitList(it)
        })

        viewModel.loadCharacters()
    }
}