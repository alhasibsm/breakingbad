package com.hasib.guskitchen.ui.characters.adapter

import androidx.recyclerview.widget.DiffUtil
import com.hasib.guskitchen.model.Character

class CharacterDiffUtil : DiffUtil.ItemCallback<Character>() {

    override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem == newItem
    }
}