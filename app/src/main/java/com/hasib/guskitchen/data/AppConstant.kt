package com.hasib.guskitchen.data

import com.hasib.guskitchen.BuildConfig

/*
 * Created by Arafin Mahtab on 7/10/2021.
 */

object AppConstant {
    val isDebug = BuildConfig.DEBUG
}
