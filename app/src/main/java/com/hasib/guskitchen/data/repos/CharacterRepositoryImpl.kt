package com.hasib.guskitchen.data.repos

import com.hasib.guskitchen.data.source.remote.CharacterApiService
import com.hasib.guskitchen.helpers.ResolveApiResponse
import com.hasib.guskitchen.helpers.Results
import com.hasib.guskitchen.model.Character
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CharacterRepositoryImpl @Inject constructor(
    private val characterApiService: CharacterApiService,
    private val resolveApiResponse: ResolveApiResponse
) : CharacterRepository {
    override suspend fun loadAllCharacters(): Results<List<Character>> =
        withContext(Dispatchers.IO) {
            return@withContext resolveApiResponse.resolve {
                characterApiService.allCharacters()
            }
        }
}