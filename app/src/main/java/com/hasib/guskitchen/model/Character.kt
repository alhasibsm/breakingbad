package com.hasib.guskitchen.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class Character(
    @field:SerializedName("char_id")
    val id: Int,
    @field:SerializedName("name")
    val name: String,
    @field:SerializedName("birthday")
    val birthday: String,
    @field:SerializedName("occupation")
    val occupation: List<String>,
    @field:SerializedName("img")
    val imgUrl: String,
    @field:SerializedName("status")
    val status: String,
    @field:SerializedName("appearance")
    val appearance: List<Int>,
    @field:SerializedName("nickname")
    val nickname: String,
    @field:SerializedName("portrayed")
    val portrayed: String
) : Parcelable
